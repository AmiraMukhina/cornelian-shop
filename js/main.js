// ANIMATION
function onEntry(entry) {
    entry.forEach(change => {
        if (change.isIntersecting) {
            change
                .target
                .classList
                .add('element-show');
        }
    });
}

let options = {
    threshold: [0.5]
};
let observer = new IntersectionObserver(onEntry, options);
let elements = document.querySelectorAll('.element-animation');

for (let elm of elements) {
    observer.observe(elm);
}

// BURGER 
const menuBtn = document.getElementById('menu-btn');
const menu = document.getElementById('nav');
const toggleMenu = function () {
    menu
        .classList
        .toggle('active-menu');
}

menuBtn.addEventListener("click", function (e) {
    menuBtn
        .classList
        .toggle('active-menu-btn');
    e.stopPropagation();
    toggleMenu();
});

document.addEventListener("click", function (e) {
    const target = e.target;
    const its_menu = target == menu || menu.contains(target);
    const its_menuBtn = target == menuBtn;
    const menu_is_active = menu
        .classList
        .contains('active-menu');

    if (!its_menu && !its_menuBtn && menu_is_active) {
        toggleMenu();
    }
});